//
//  PostAuthor+CoreDataProperties.swift
//  VkNewsFeed
//
//  Created by Petko Todorov on 6/6/18.
//  Copyright © 2018 Petko Todorov. All rights reserved.
//
//

import Foundation
import CoreData


extension PostAuthor {
    
    convenience init?(withJson json: [String: Any], forGroup: Bool, in context: NSManagedObjectContext) {
        
        guard let authorId = json["id"] as? Int64,
            let avatarUrl = json["photo_50"] as? String else { return nil }
        
        let name: String
        if forGroup {
            guard let jsonName = json["name"] as? String else { return nil }
            name = jsonName
        } else {
            guard let firstName = json["first_name"] as? String,
                let lastName = json["last_name"] as? String else { return nil }
            name = "\(firstName) \(lastName)"
        }
        
        let entity = NSEntityDescription.entity(forEntityName: "PostAuthor", in: context)!
        self.init(entity: entity, insertInto: context)
        
        self.authorId = authorId
        self.avatarUrl = avatarUrl
        self.displayName = name
    }

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PostAuthor> {
        return NSFetchRequest<PostAuthor>(entityName: "PostAuthor")
    }

    @NSManaged public var authorId: Int64
    @NSManaged public var displayName: String?
    @NSManaged public var avatarUrl: String?

}
