//
//  Photo+CoreDataProperties.swift
//  VkNewsFeed
//
//  Created by Petko Todorov on 6/6/18.
//  Copyright © 2018 Petko Todorov. All rights reserved.
//
//

import Foundation
import CoreData


extension Photo {
    
    convenience init?(withJson json: [String: Any], in context: NSManagedObjectContext) {
        
        guard let type = json["type"] as? String, type == "photo" else { return nil }
        guard let photoObj = json["photo"] as? [String: Any] else { return nil }
        guard let photoId = photoObj["id"] as? Int64 else { return nil }
        guard let photoUrl = photoObj["photo_604"] as? String else { return nil }
        
        let entity = NSEntityDescription.entity(forEntityName: "Photo", in: context)!
        self.init(entity: entity, insertInto: context)
        
        self.photoId = photoId
        self.photoUrl = photoUrl
    }

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Photo> {
        return NSFetchRequest<Photo>(entityName: "Photo")
    }

    @NSManaged public var photoId: Int64
    @NSManaged public var photoUrl: String?

}
