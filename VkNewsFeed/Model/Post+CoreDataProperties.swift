//
//  Post+CoreDataProperties.swift
//  VkNewsFeed
//
//  Created by Petko Todorov on 6/6/18.
//  Copyright © 2018 Petko Todorov. All rights reserved.
//
//

import Foundation
import CoreData


extension Post {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Post> {
        return NSFetchRequest<Post>(entityName: "Post")
    }

    @NSManaged public var postId: Int64
    @NSManaged public var sourceId: Int64
    @NSManaged public var date: Double
    @NSManaged public var text: String?
    @NSManaged public var comments: Int32
    @NSManaged public var likes: Int32
    @NSManaged public var reposts: Int32
    @NSManaged public var author: PostAuthor?
    @NSManaged public var photos: NSOrderedSet?

}

// MARK: Generated accessors for photos
extension Post {
    
    convenience init?(withJson json: [String: Any], in context: NSManagedObjectContext) {
        
        guard let postId = json["post_id"] as? Int64 else { return nil }
        guard let date = json["date"] as? TimeInterval else { return nil }
        guard let text = json["text"] as? String else { return nil }
        guard let sourceId = json["source_id"] as? Int64 else { return nil }
        guard let commentsObj = json["comments"] as? [String: Any],
            let comments = commentsObj["count"] as? Int32 else { return nil }
        guard let likesObj = json["likes"] as? [String: Any],
            let likes = likesObj["count"] as? Int32 else { return nil }
        guard let repostsObj = json["reposts"] as? [String: Any],
            let reposts = repostsObj["count"] as? Int32 else { return nil }
        
        var jsonPhotos = [Photo]()
        if let attachments = json["attachments"] as? [[String: Any]] {
            attachments.forEach {
                if let photo = Photo(withJson: $0, in: context) {
                    jsonPhotos.append(photo)
                }
            }
        }
        
        let entity = NSEntityDescription.entity(forEntityName: "Post", in: context)!
        self.init(entity: entity, insertInto: context)
        
        self.postId = postId
        self.date = date
        self.text = text
        self.sourceId = sourceId < 0 ? sourceId / -1 : sourceId
        self.comments = comments
        self.likes = likes
        self.reposts = reposts
        
        self.addToPhotos(NSOrderedSet(array: jsonPhotos))
    }

    @objc(insertObject:inPhotosAtIndex:)
    @NSManaged public func insertIntoPhotos(_ value: Photo, at idx: Int)

    @objc(removeObjectFromPhotosAtIndex:)
    @NSManaged public func removeFromPhotos(at idx: Int)

    @objc(insertPhotos:atIndexes:)
    @NSManaged public func insertIntoPhotos(_ values: [Photo], at indexes: NSIndexSet)

    @objc(removePhotosAtIndexes:)
    @NSManaged public func removeFromPhotos(at indexes: NSIndexSet)

    @objc(replaceObjectInPhotosAtIndex:withObject:)
    @NSManaged public func replacePhotos(at idx: Int, with value: Photo)

    @objc(replacePhotosAtIndexes:withPhotos:)
    @NSManaged public func replacePhotos(at indexes: NSIndexSet, with values: [Photo])

    @objc(addPhotosObject:)
    @NSManaged public func addToPhotos(_ value: Photo)

    @objc(removePhotosObject:)
    @NSManaged public func removeFromPhotos(_ value: Photo)

    @objc(addPhotos:)
    @NSManaged public func addToPhotos(_ values: NSOrderedSet)

    @objc(removePhotos:)
    @NSManaged public func removeFromPhotos(_ values: NSOrderedSet)

}
