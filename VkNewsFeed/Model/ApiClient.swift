//
//  ApiClient.swift
//  VkNewsFeed
//
//  Created by Petko Todorov on 6/05/18.
//  Copyright © 2018 Petko Todorov. All rights reserved.
//

import Foundation
import VK_ios_sdk

class ApiClient {
    
    init(withCache cache: NSCache<NSString, UIImage>) {
        self.imageCache = cache
    }
    
    let imageCache: NSCache<NSString, UIImage>
    
    func fetchNewsFeed(withParameters parameters: [String: Any], completion: @escaping(VKResponse<VKApiObject>?, Error?) -> Void) {
        let method = "newsfeed.get"
        let request = VKRequest(method: method, parameters: parameters)
        request?.execute(resultBlock: { (result) in
            completion(result, nil)
        }, errorBlock: { (error) in
            completion(nil, error)
        })
    }
    
    func fetchImage(forUrl urlString: String?, completion: @escaping (UIImage?) -> Void) {
        guard let urlString = urlString else {
            completion(nil)
            return
        }
        if let image = imageCache.object(forKey: urlString as NSString) {
            completion(image)
        } else if let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url) { (data, _, error) in
                if let error = error {
                    print(error)
                }
                if let data = data, let image = UIImage(data: data){
                    self.imageCache.setObject(image, forKey: urlString as NSString)
                    completion(image)
                }
            }.resume()
        }
    }
    
    
    
}
