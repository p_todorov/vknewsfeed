//
//  Group.swift
//  VkNewsFeed
//
//  Created by Petko Todorov on 5/28/18.
//  Copyright © 2018 Petko Todorov. All rights reserved.
//

import Foundation
import RealmSwift

class PostAuthorRealm: Object {
//    @objc dynamic var authorId = 0
//    @objc dynamic var avatarUrl = ""
//
//    convenience init?(withJson json: [String: Any]) {
//        self.init()
//        guard let authorId = json["id"] as? Int,
//            let avatarUrl = json["photo_50"] as? String else { return nil }
//
//        self.authorId = authorId
//        self.avatarUrl = avatarUrl
//    }
}

class Group: PostAuthorRealm {
    
    
    @objc dynamic var groupId = 0
    @objc dynamic var name = ""
    @objc dynamic var avatarUrl = ""
    
    convenience init?(withJson json: [String: Any]) {
        self.init()
        guard let groupId = json["id"] as? Int,
            let name = json["name"] as? String,
            let avatarUrl = json["photo_50"] as? String else { return nil }
        
        self.groupId = groupId
        self.name = name
        self.avatarUrl = avatarUrl
    }
    
}

extension Group: PostAuthor {
        
    var authodId: Int {
        return groupId
    }
    
    var displayName: String {
        return name
    }
    
    var avatar: String {
        return avatarUrl
    }
    
}
