//
//  ResponseParser.swift
//  VkNewsFeed
//
//  Created by Petko Todorov on 5/30/18.
//  Copyright © 2018 Petko Todorov. All rights reserved.
//

import Foundation
import CoreData

struct ResponseParser {
    
    static func parseResponse(_ response: Any?, in context: NSManagedObjectContext) -> (posts: [Post], nextFrom: String) {
        guard let json = response as? [String: Any],
            let items = json["items"] as? [[String: Any]],
            let profiles = json["profiles"] as? [[String: Any]],
            let groups = json["groups"] as? [[String: Any]],
            let nextFrom = json["next_from"] as? String else {
                return ([Post](),"")
                
        }
        
        var authors = [PostAuthor]()
        var posts = [Post]()
        profiles.forEach {
            if let profile = PostAuthor(withJson: $0, forGroup: false, in: context){
                authors.append(profile)
            }
        }
        groups.forEach {
            if let group = PostAuthor(withJson: $0, forGroup: true, in: context) {
                authors.append(group)
            }
        }
        items.forEach{
            if let post = Post(withJson: $0, in: context) {
                post.author = authors.first { $0.authorId == post.sourceId }
                posts.append(post)
            }
        }
        return (posts, nextFrom)
        
    }
    
}
