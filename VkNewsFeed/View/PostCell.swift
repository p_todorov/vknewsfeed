//
//  PostCellTableViewCell.swift
//  VkNewsFeed
//
//  Created by Petko Todorov on 6/05/18.
//  Copyright © 2018 Petko Todorov. All rights reserved.
//

import UIKit

class PostCell: UITableViewCell {
    
    var apiClient: ApiClient!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        headerView.avatarImgView.image = nil
        photosView.photos = [#imageLiteral(resourceName: "placeholder"), #imageLiteral(resourceName: "placeholder")]
    }
    
    let headerViewHeight: CGFloat = 60
    let txtViewHeight: CGFloat = 50
    
    var post: Post? {
        didSet {
            guard let post = post,
            let author = post.author else { return }
            headerView.nameLabel.text = author.displayName
            
            let postDate = Date(timeIntervalSince1970: post.date)
            headerView.dateLabel.text = postDate.dateTimeString
            if let text = post.text {
                postTextView.isHidden = text.count > 0 ? false : true
                postTextView.text = text
            }
            setAvatar(forUrl: author.avatarUrl)
            if let photos = post.photos {
                setPhotos(photos.array as? [Photo])
            }
        }
    }
    
    private let headerView: CellHeaderView = {
        return CellHeaderView(frame: .zero)
    }()
    
    private let verticalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 0
        return stackView
    }()
    
    private let postTextView: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    private let photosView: PhotosView = {
        return PhotosView(frame: .zero)
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layoutViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func layoutViews() {
        
        addSubview(headerView)
        _ = headerView.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: headerViewHeight)
        
        addSubview(verticalStackView)
        verticalStackView.anchorTo(top: headerView.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor)
        
        verticalStackView.addArrangedSubview(postTextView)
        let constr = postTextView.heightAnchor.constraint(equalToConstant: txtViewHeight)
        constr.priority = .init(999)
        constr.isActive = true
        
        verticalStackView.addArrangedSubview(photosView)
    }
    
    private func setPhotos(_ photos: [Photo]?) {
        guard let photos = photos, photos.count > 0 else {
            photosView.isHidden = true
            return
        }
        photosView.isHidden = false
        let dispatchGroup = DispatchGroup()
        var images = [UIImage]()
        
        //max 2 photos
        let endIndex = photos.count >= 2 ? 1 : 0
        
        for i in 0...endIndex {
                dispatchGroup.enter()
                apiClient.fetchImage(forUrl: photos[i].photoUrl) { (image) in
                    if let image = image {
                        images.append(image)
                    }
                    dispatchGroup.leave()
                }
        }
        
        dispatchGroup.notify(queue: .main) {
            self.photosView.photos = images
        }
        
    }
    
    private func setAvatar(forUrl urlString: String?) {
        guard let urlString = urlString else { return }
        apiClient.fetchImage(forUrl: urlString, completion: { (image) in
            if let image = image {
                DispatchQueue.main.async {
                    self.headerView.avatarImgView.image = image
                }
            }
        })
    }

}

