//
//  CellHeaderView.swift
//  VkNewsFeed
//
//  Created by Petko Todorov on 6/05/18.
//  Copyright © 2018 Petko Todorov. All rights reserved.
//

import UIKit

class CellHeaderView: UIView {
    
    let avatarHeightConstant: CGFloat = 50
    let padding: CGFloat = 5

    lazy var avatarImgView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = self.avatarHeightConstant / 2
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = UIColor.customblue
        label.font = UIFont.boldSystemFont(ofSize: 13)
        return label
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(avatarImgView)
        addSubview(nameLabel)
        addSubview(dateLabel)
        
        _ = avatarImgView.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil
            , topConstant: padding, leftConstant: padding, bottomConstant: 0, rightConstant: 0, widthConstant: avatarHeightConstant, heightConstant: avatarHeightConstant)
        _ = nameLabel.anchor(top: avatarImgView.topAnchor, left: avatarImgView.rightAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: padding, bottomConstant: 0, rightConstant: padding, widthConstant: 0, heightConstant: avatarHeightConstant/2)
        _ = dateLabel.anchor(top: nameLabel.bottomAnchor, left: avatarImgView.rightAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: padding, bottomConstant: 0, rightConstant: padding, widthConstant: 0, heightConstant: avatarHeightConstant/2)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
