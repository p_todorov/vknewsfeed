//
//  PhotosView.swift
//  VkNewsFeed
//
//  Created by Petko Todorov on 6/05/18.
//  Copyright © 2018 Petko Todorov. All rights reserved.
//

import UIKit

class PhotosView: UIView {
    
    var photos = [UIImage]() {
        didSet {
            self.isHidden = photos.count > 0 ? false : true
            if photos.count > 0 {
                firstImgView.image = photos[0]
                if photos.count > 1 {
                    secondImgView.isHidden = false
                    secondImgView.image = photos[1]
                } else {
                    secondImgView.isHidden = true
                }
            }
        }
    }
    
    private lazy var photosStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [firstImgView, secondImgView])
        stack.spacing = 5
        stack.distribution = .fillProportionally
        stack.axis = .horizontal
        return stack
    }()
    
    private let firstImgView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "placeholder")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let secondImgView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "placeholder")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(photosStackView)
        photosStackView.anchorTo(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor)
//        photosStackView.anchorWithConstantsTo(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 5, leftConstant: 5, bottomConstant: 5, rightConstant: 5)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
