//
//  NewsViewController.swift
//  VkNewsFeed
//
//  Created by Petko Todorov on 6/05/18.
//  Copyright © 2018 Petko Todorov. All rights reserved.
//

import UIKit
import VK_ios_sdk
import CoreData

class NewsController: UITableViewController {
    
    lazy var dateSortDescriptor: NSSortDescriptor = {
        return NSSortDescriptor(
            key: #keyPath(Post.date),
            ascending: false)
    }()
    
    var fetchRequest: NSFetchRequest<Post>!
    var asyncFetchRequest: NSAsynchronousFetchRequest<Post>!
    
    var managedContext: NSManagedObjectContext
    
    private var posts = [Post]()
    
    private var nextFrom = ""
    private var isLoading = true
    
    let apiClient: ApiClient
    
    init(withApiClient apiClient: ApiClient, andManagedContext context: NSManagedObjectContext) {
        self.apiClient = apiClient
        self.managedContext = context
        super.init(style: .plain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeTableView()
        fetchResultsFromStorage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refreshDataSource(withOlderPosts: false)
    }
    
    private func customizeTableView() {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.customblue
        let attributes = [NSAttributedStringKey.foregroundColor: UIColor.customblue]
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching Posts ...", attributes: attributes)
        refreshControl.addTarget(self, action: #selector(refreshDataSource), for: UIControlEvents.valueChanged)
        self.refreshControl = refreshControl
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        let navItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_account_circle"), style: .plain, target: self, action: #selector(onProfile))
        navigationItem.leftBarButtonItem = navItem
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
        tableView.separatorStyle = .none
        tableView.register(PostCell.self, forCellReuseIdentifier: PostCell.reuseIdentifier)
    }
    
    private func fetchResultsFromStorage() {
        fetchRequest = Post.fetchRequest()
        fetchRequest.sortDescriptors = [dateSortDescriptor]
        
        asyncFetchRequest =
            NSAsynchronousFetchRequest<Post>(
            fetchRequest: fetchRequest) {
                [unowned self] (result: NSAsynchronousFetchResult) in
                guard let posts = result.finalResult else {
                    return
                }
                self.posts = posts
        }
        do {
            try managedContext.execute(asyncFetchRequest)
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    @objc private func onProfile() {
        guard let currentUser = VKSdk.accessToken().localUser else {
            navigationController?.popToRootViewController(animated: true)
            return
        }
        
        let profileController = ProfileController(withUser: currentUser, andApiClient: apiClient)
        navigationController?.pushViewController(profileController, animated: true)
    }
    
    @objc private func refreshDataSource(withOlderPosts: Bool) {
        
        var parameters = ["filters": "post"] as [String : Any]
        
        if withOlderPosts == true && !nextFrom.isEmpty {
            parameters["start_from"] = nextFrom
        }
        
        showLoading(flag: true)
        apiClient.fetchNewsFeed(withParameters: parameters) { (response, error) in
            self.nextFrom = ""
            self.isLoading = false
            self.refreshControl?.endRefreshing()
            self.showLoading(flag: false)
            
            if let error = error {
                if self.posts.count > 0 {
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                self.showAlert(withTitle: "Error", withMessage: error.localizedDescription)
            } else if let response = response {
                
                let parsedResult = ResponseParser.parseResponse(response.json, in: self.managedContext)
                
                self.nextFrom = parsedResult.nextFrom
                let index = withOlderPosts ? self.posts.count - 1 : 0
                self.posts.insert(contentsOf: parsedResult.posts, at: index)
                
                do {
                    try self.managedContext.save()
                } catch let error as NSError {
                    print("Could not save \(error), \(error.userInfo)")
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }

        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tableView.contentOffset.y - (tableView.contentSize.height - tableView.frame.size.height) > 30 && !isLoading {
            isLoading = true
            refreshDataSource(withOlderPosts: true)
        }
    }
}

extension NewsController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let detailsController = PostDetailsController(withPost: posts[indexPath.row], andApiClient: apiClient)
        detailsController.title = "Post Details"
        navigationController?.pushViewController(detailsController, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PostCell = tableView.dequeueReusableCell(for: indexPath)
        cell.apiClient = self.apiClient
        cell.post = posts[indexPath.row]
        return cell
    }
    
}
