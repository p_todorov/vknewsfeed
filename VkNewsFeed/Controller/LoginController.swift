//
//  LoginController.swift
//  VkNewsFeed
//
//  Created by Petko Todorov on 6/05/18.
//  Copyright © 2018 Petko Todorov. All rights reserved.
//

import UIKit
import VK_ios_sdk

class LoginController: UIViewController {
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "VK News Feed"
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.textColor = UIColor.customblue
        return label
    }()
    
    let signInButton: UIButton = {
        let button = UIButton(type: UIButtonType.custom)
        button.layer.cornerRadius = 10
        button.layer.borderWidth = 1
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Sign in", for: .normal)
        button.backgroundColor = UIColor.customGreen
        button.addTarget(self, action: #selector(onSignIn), for: .touchUpInside)
        return button
    }()
    
    let scope = [VK_PER_WALL, VK_PER_FRIENDS]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        layoutViews()
        
        VKSdk.initialize(withAppId: "6492074").register(self)
        VKSdk.instance().uiDelegate = self
        VKSdk.wakeUpSession(scope) { (state, error) in
            if state == .authorized {
                self.successfulAuthentication()
            } else if error != nil {
                self.showAlert(withTitle: nil, withMessage: "Access denied\n \(error!.localizedDescription)")
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func layoutViews() {
        view.addSubview(titleLabel)
        _ = titleLabel.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 100, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 40)
        view.addSubview(signInButton)
        _ = signInButton.anchor(top: nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 200, heightConstant: 60)
        signInButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        signInButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    private func successfulAuthentication() {
        
        let cache = NSCache<NSString, UIImage>()
        let apiClient = ApiClient(withCache: cache)
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let newsController = NewsController(withApiClient: apiClient, andManagedContext: delegate.coreDataStack.managedContext)
        newsController.title = "News feed"
        navigationController?.pushViewController(newsController, animated: true)
    }
    
    @objc private func onSignIn() {
        VKSdk.authorize(scope)
    }
    
}

extension LoginController: VKSdkDelegate {
    
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        
        if result.token != nil {
            successfulAuthentication()
        } else if result.error != nil {
            self.showAlert(withTitle: nil, withMessage: "Access denied\n \(result.error.localizedDescription)")
        }
    }
    
    func vkSdkUserAuthorizationFailed() {
        self.showAlert(withTitle: nil, withMessage: "Access denied")
        navigationController?.popToRootViewController(animated: true)
    }
    
}

extension LoginController:  VKSdkUIDelegate {
    
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        navigationController?.topViewController?.present(controller, animated: true, completion: nil)
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        let captchaVc = VKCaptchaViewController.captchaControllerWithError(captchaError)
        captchaVc?.present(in: navigationController?.topViewController)
    }
    
}
