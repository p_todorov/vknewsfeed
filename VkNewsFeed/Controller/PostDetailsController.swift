//
//  PostDetailsController.swift
//  VkNewsFeed
//
//  Created by Petko Todorov on 6/06/18.
//  Copyright © 2018 Petko Todorov. All rights reserved.
//

import UIKit

class PostDetailsController: UIViewController {
    
    let post: Post
    let apiClient: ApiClient
    
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = .white
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 0)
        return scrollView
    }()
    
    private let headerView: CellHeaderView = {
        return CellHeaderView(frame: .zero)
    }()
    
    private let postTextView: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        textView.isEditable = false
        textView.isScrollEnabled = false
        return textView
    }()
    
    private lazy var horizontalStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews:
            [labelWithText("Likes", andValue: Int(post.likes)),
             labelWithText("Comments", andValue: Int(post.comments)),
             labelWithText("Reposts", andValue: Int(post.reposts))])
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillProportionally
        stackView.spacing = 10
        return stackView
    }()
    
    private let verticalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .fillProportionally
        stackView.spacing = 5
        return stackView
    }()
    
    init(withPost post: Post, andApiClient apiClient: ApiClient) {
        self.post = post
        self.apiClient = apiClient
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
    }
    
    private func setAvatar(forUrl urlString: String?) {
        apiClient.fetchImage(forUrl: urlString, completion: { (image) in
            if let image = image {
                DispatchQueue.main.async {
                    self.headerView.avatarImgView.image = image
                }
            }
        })
    }
    
}

//MARK: Layout of views
extension PostDetailsController {
    
    private func layoutViews() {
        view.addSubview(scrollView)
        scrollView.anchorTo(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor)
        addHeaderView()
        addTextView()
        addVerticalStack()
    }
    
    private func addHeaderView() {
        scrollView.addSubview(headerView)
        _ = headerView.anchor(top: scrollView.topAnchor, left: scrollView.leftAnchor, bottom: nil, right: scrollView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 60)
        headerView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1).isActive = true
        
        if let author = post.author {
            headerView.nameLabel.text = author.displayName
            let postDate = Date(timeIntervalSince1970: post.date)
            headerView.dateLabel.text = postDate.dateTimeString
            setAvatar(forUrl: author.avatarUrl)
        }
    }
    
    private func addTextView() {
        if post.text != nil && post.text!.count > 0 {
            scrollView.addSubview(postTextView)
            postTextView.anchorWithConstantsTo(top: headerView.bottomAnchor, left: scrollView.leftAnchor, bottom: nil, right: scrollView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0)
            postTextView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1).isActive = true
            postTextView.text = post.text
        }
    }
    
    private func addVerticalStack() {
        let notEmpty = post.text != nil && post.text!.count > 0
        let stackTopAnchor = notEmpty ? postTextView.bottomAnchor : headerView.bottomAnchor
        scrollView.addSubview(verticalStackView)
        verticalStackView.anchorTo(top: stackTopAnchor, left: scrollView.leftAnchor, bottom: scrollView.bottomAnchor, right: scrollView.rightAnchor)
        
        if let photos = post.photos?.array as? [Photo], !photos.isEmpty {
            for photo in photos {
                let imageView = photoImageView()
                verticalStackView.addArrangedSubview(imageView)
                apiClient.fetchImage(forUrl: photo.photoUrl) { (image) in
                    guard let image = image else { return }
                    DispatchQueue.main.async {
                        imageView.image = image
                    }
                    
                }
            }
        }
        
        verticalStackView.addArrangedSubview(horizontalStackView)
        horizontalStackView.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
}

//MARK: Creatiomn of common views
extension PostDetailsController {
    
    private func photoImageView() -> UIImageView {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "placeholder")
        iv.contentMode = .scaleAspectFit
        return iv
    }
    
    private func labelWithText(_ text: String, andValue value: Int) -> UILabel {
        let label = UILabel()
        label.numberOfLines = 1
        label.textAlignment = .center
        label.textColor = UIColor.customblue
        label.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        label.text = text + ": \(value)"
        return label
    }
    
}
