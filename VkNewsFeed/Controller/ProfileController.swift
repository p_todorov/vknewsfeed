//
//  ProfileController.swift
//  VkNewsFeed
//
//  Created by Petko Todorov on 6/06/18.
//  Copyright © 2018 Petko Todorov. All rights reserved.
//

import UIKit
import VK_ios_sdk

class ProfileController: UIViewController {
    
    let currentUser: VKUser
    let apiClient: ApiClient

    let avatarImageWidth: CGFloat = 150
    
    lazy var avatarImgView: UIImageView = {
        let imgView = UIImageView()
        imgView.layer.cornerRadius = avatarImageWidth / 2
        imgView.layer.masksToBounds = true
        imgView.layer.borderWidth = 2
        imgView.image = #imageLiteral(resourceName: "placeholder")
        imgView.contentMode = .scaleAspectFit
        return imgView
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.medium)
        return label
    }()
    
    let signOutBtn: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.customGreen
        button.layer.cornerRadius = 10
        button.setTitle("Sign Out", for: .normal)
        button.setTitleColor(.red, for: .normal)
        button.addTarget(self, action: #selector(signOut), for: .touchUpInside)
        return button
    }()
    
    init(withUser user: VKUser, andApiClient apiClient: ApiClient) {
        self.currentUser = user
        self.apiClient = apiClient
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutViews()
        setAvatar()
        setName()
    }
    
    @objc private func signOut() {
        showAlert(withTitle: "Sign out?", withMessage: "Are you sure you want to sign out?", defaultButtonTitle: "Sign Out!") { (_) in
            VKSdk.forceLogout()
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    private func setAvatar() {
        guard let url = currentUser.photo_200 else { return }
        apiClient.fetchImage(forUrl: url) { (image) in
            if let image = image {
                DispatchQueue.main.async {
                    self.avatarImgView.image = image
                }
            }
        }
    }
    
    private func setName() {
        var name = ""
        if let firstName = currentUser.first_name {
            name = firstName
        }
        if let lastName = currentUser.last_name {
            name.append(" \(lastName)")
        }
        nameLabel.text = name
    }
    
    private func layoutViews() {
        view.backgroundColor = .white
        
        view.addSubview(avatarImgView)
        _ = avatarImgView.anchor(top: topLayoutGuide.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 40, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: avatarImageWidth, heightConstant: avatarImageWidth)
        avatarImgView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        view.addSubview(nameLabel)
        nameLabel.anchorWithConstantsTo(top: avatarImgView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 30, leftConstant: 16, bottomConstant: 0, rightConstant: 16)
        
        view.addSubview(signOutBtn)
        _ = signOutBtn.anchor(top: nil, left: nil, bottom: view.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 200, heightConstant: 50)
        signOutBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    
}
