//
//  UIViewController+Alerts.swift
//  VkNewsFeed
//
//  Created by Petko Todorov on 6/05/18.
//  Copyright © 2018 Petko Todorov. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlert(withTitle title: String?,
                   withMessage message: String,
                   defaultButtonTitle buttonTitle: String = "Ok",
                   buttonCallback callback: ((UIAlertAction) -> Void)? = nil) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: callback))
            if callback != nil {
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            }
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func showLoading(flag: Bool) {
        DispatchQueue.main.async {
            if flag {
                self.view.isUserInteractionEnabled = false
                //check if indicator is created and reuse it
                let subview = self.view.subviews.first(where: { (item) -> Bool in
                    item is UIActivityIndicatorView
                })
                if let subview = subview as? UIActivityIndicatorView {
                    subview.startAnimating()
                } else {
                    //create instance
                    let indicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
                    indicator.hidesWhenStopped = true
                    indicator.color = UIColor.customblue
                    self.view.addSubview(indicator)
                    self.view.bringSubview(toFront: indicator)
                    indicator.translatesAutoresizingMaskIntoConstraints = false
                    indicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
                    indicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
                    indicator.startAnimating()
                }
            } else {
                //check if indicator is created and stop it
                self.view.isUserInteractionEnabled = true
                let subview = self.view.subviews.first(where: { (item) -> Bool in
                    item is UIActivityIndicatorView
                })
                if let subview = subview as? UIActivityIndicatorView {
                    subview.stopAnimating()
                }
            }
        }
    }
    
}

