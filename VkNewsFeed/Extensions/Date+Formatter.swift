//
//  Date+formatter.swift
//  VkNewsFeed
//
//  Created by Petko Todorov on 6/05/18.
//  Copyright © 2018 Petko Todorov. All rights reserved.
//

import Foundation

extension Date {
    
    private struct Formatter {
        
        static let dateFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone(abbreviation: "GMT")
            formatter.locale = NSLocale.current
            formatter.dateFormat = "dd-MM-yyyy 'at' HH:mm"
            return formatter
        }()
        
    }
    
    var dateTimeString: String {
        return Formatter.dateFormatter.string(from: self)
    }
    
}
